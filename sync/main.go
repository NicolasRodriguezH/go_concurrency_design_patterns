package main

import (
	"fmt"
	"sync"
)

/* Variable global, y todas las goRoutines acceden a este valor al tiempo */
/* Y se presenta el Race Condition, para esto se usa Lock() */
var (
	balance int = 100
)

func Deposite(ammont int, wg *sync.WaitGroup, lock *sync.RWMutex) {
	defer wg.Done()
	/* Con lock se eliminan warnings de race condition, bloquea el programa y unlock desbloquea */
	/* mientras balance obtiene el valor que debe tener */
	lock.Lock()
	b := balance
	balance = b + ammont
	lock.Unlock()
}

func Balance(lock *sync.RWMutex) int {
	lock.RLock()
	b := balance
	lock.RUnlock()
	return b
}

func main() {
	var wg sync.WaitGroup
	var lock sync.RWMutex
	/* Emular depositos que ocurren sobre balance en un mismo periodo de timepo */
	for i := 0; i <= 5; i++ {
		wg.Add(1)
		go Deposite(i*100, &wg, &lock)
	}
	wg.Wait()
	fmt.Println(Balance(&lock))
}
