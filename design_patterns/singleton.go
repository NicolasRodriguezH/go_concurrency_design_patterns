package main

import (
	"fmt"
	"sync"
	"time"
)

/* El caso de DDBB es el mas usado para al aplicar el Singleton */

/* Struct equivalente a las clases en GO */
type Database struct {
}

func (Database) CreateSingleConnetcion() {
	fmt.Println("Creating Singleton for database")
	time.Sleep(2 * time.Second)
	fmt.Println("Creaation Done")
}

var db *Database
var lock sync.Mutex

func getDatabaseInstance() *Database {
	lock.Lock()
	defer lock.Unlock()
	if db == nil {
		fmt.Println("Creating database connection")
		db = &Database{}
		db.CreateSingleConnetcion()
	}

	/* En caso de que ya se haya instanciado se salta el if y pasa aca */
	fmt.Println("DB Already Created")

	return db
}

func main() {
	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			getDatabaseInstance()
		}()
	}
	wg.Wait()
}
