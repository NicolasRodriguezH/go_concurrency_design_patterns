package main

import "fmt"

//For Shop - Creacional // Factory

//SuperClase o Interface - IProduct siepmre va a ser manejado de la misma manera, eso permite el patro de diseño Factory
type IProduct interface {
	setStock(stock int)
	getStock() int
	setName(n string)
	getName() string
}

//A partir de este struct (Clase base), creo las demas subclases, esto puede ser de Ropa, Comida, etc..
//Satisface las interfaces previamente creadas
type Computer struct {
	name  string
	stock int
}

//El metodo recibe el struct Computer y define los emtodos de la interface para incluirla implicitamente
//Subtipos especificos implementados
func (c *Computer) setStock(stock int) {
	c.stock = stock
}

func (c *Computer) setName(n string) {
	c.name = n
}

func (c *Computer) getStock() int {
	return c.stock
}

func (c *Computer) getName() string {
	return c.name
}

//Composicion sobre herencia en GO, equivalente a la herencia en POO
type Laptop struct {
	Computer
}

//Contructor para Laptop
func NewLaptop() IProduct {
	return &Laptop{
		Computer: Computer{
			name:  "Laptop Computer",
			stock: 34,
		},
	}
}

type Desktop struct {
	Computer
}

//Contructor para Desktop
func newDesktop() IProduct {
	return &Desktop{
		Computer: Computer{
			name:  "Desktop Computer",
			stock: 21,
		},
	}
}

//Creacional // Factory
func GetComputerFactory(ComputerType string) (IProduct, error) {
	if ComputerType == "Laptop" {
		return NewLaptop(), nil
	}

	if ComputerType == "Desktop" {
		return newDesktop(), nil
	}

	return nil, fmt.Errorf("Product not found")
}

//Se da el polimorfismo en el comportamiento, no vamos a tener que escribir este metodo una y otra vez.
//Por cada producto que vayamos creando
func printNameAndStock(p IProduct) {
	fmt.Printf("Product name: %s, with stock: %d\n", p.getName(), p.getStock())
}

func setNameAndStock(p IProduct) {
	p.setName("Comida")
	p.setStock(20)
}

func main() {
	/* Se omite el err, ya que estamos en un ambiente controlado */
	laptop, _ := GetComputerFactory("Laptop")
	/* if err != nil {
		fmt.Println(err)
	} */
	desktop, _ := GetComputerFactory("Desktop")
	/* fmt.Printf("%v\n", laptop)
	fmt.Printf("%v\n", desktop) */
	printNameAndStock(laptop)
	printNameAndStock(desktop)
}
