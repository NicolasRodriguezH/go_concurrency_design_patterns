package main

import "fmt"

/* Adapter te sirve para usar un struct o clase de la forma mas correcta por medio de interface */
/* Evitando refactors y escribir codigo innecesario */

type Payment interface {
	Pay()
}

type CashPayment struct {
}

func (CashPayment) Pay() {
	fmt.Println("Payment using Cash")
}

func ProcessPayment(p Payment) {
	p.Pay()
}

type BankPayment struct {
}

func (BankPayment) Pay(bankAccount int) {
	fmt.Println("Paying using bank account: %d\n", bankAccount)
}

//Adapter
type BankPaymentAdapter struct {
	BankPayment *BankPayment
	bankAccount int
}

func (bpa *BankPaymentAdapter) Pay() {
	bpa.BankPayment.Pay(bpa.bankAccount)
}

func main() {
	cash := &CashPayment{}
	ProcessPayment(cash)

	/* bank := &BankPayment{}
	ProcessPayment(bank) */
	bpa := &BankPaymentAdapter{
		bankAccount: 5,
		/* Instancia nueva de BankPayment */
		BankPayment: &BankPayment{},
	}
	ProcessPayment(bpa)
}
